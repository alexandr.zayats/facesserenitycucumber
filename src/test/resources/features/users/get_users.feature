Feature: Get user/users requests

  Scenario Outline: Get existed user
    When Get existed user with id <ID>
    Then First Name is equal "<firstName>"
    And Last Name is equal "<lastName>"
    Examples:
      | ID | firstName | lastName |
      | 2  | Janet     | Weaver   |
      | 3  | Emma      | Wong     |

  Scenario: Get non-existed user
    When Get non-existed user with id 0
    Then Status code 404 will be returned

  Scenario: Get page with users
    When Get page 2 with users
    Then List with 6 users
    And On the page 2