package facesapi.steps;

import com.facesapi.api.UsersApi;
import com.facesapi.model.UsersResponse;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class UsersSteps {

    @Steps
    UsersApi usersApi;

    @Steps
    UsersResponse usersResponse;

    @When("Get existed user with id {int}")
    public void getExistedUserWithId(int id) {
        usersApi.getUserById(id);
    }

    @Then("First Name is equal {string}")
    public void firstNameIsEqual(String firstName) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body(usersResponse.getFirstName(), equalTo(firstName)));
    }

    @And("Last Name is equal {string}")
    public void lastNameIsEqual(String lastName) {
        restAssuredThat(response -> response.body(usersResponse.getLastName(), equalTo(lastName)));
    }

    @When("Get page {int} with users")
    public void getPageWithUsers(int page) {
        usersApi.getListOfUsersByPage(page);
    }

    @Then("List with {int} users")
    public void listWithUsers(int amountOfUsersOnPage) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body(usersResponse.getPerPage(), equalTo(amountOfUsersOnPage)));
    }

    @And("On the page {int}")
    public void onThePage(int page) {
        restAssuredThat(response -> response.body(usersResponse.getPage(), equalTo(page)));
    }

    @When("Get non-existed user with id {int}")
    public void getNonExistedUserWithId(int id) {
        usersApi.getUserById(id);
    }

    @Then("Status code {int} will be returned")
    public void statusCodeReturned(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }
}