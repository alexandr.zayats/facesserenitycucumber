package com.facesapi.model;

import lombok.Data;

@Data
public class UsersResponse {
    public String firstName = "data.first_name";
    public String lastName = "data.last_name";
    public String page = "page";
    public String perPage = "per_page";
}