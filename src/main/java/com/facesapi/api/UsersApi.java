package com.facesapi.api;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;

public class UsersApi {
    public EnvironmentVariables environmentVariables;

    private static final String USERS_URL = "users";
    private static final String USERS_PAGE_URL = "?page=";

    @Step
    public String getEnv() {
        return EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("base.api");
    }

    @Step
    public void getUserById(int id) {
        SerenityRest.given().get(String.format("%s%s/%d", getEnv(), USERS_URL, id));
    }

    @Step
    public void getListOfUsersByPage(int page) {
        SerenityRest.given().get(String.format("%s%s%s%d", getEnv(), USERS_URL, USERS_PAGE_URL, page));
    }
}