# FacesSerenityCucumber

FacesSerenityCucumber project is a test suite to verify the [Faces API](https://reqres.in/).

In this Test Suite we will test:
1) getting user data with correct ID;
2) getting user data with incorrect ID;
3) getting list of users on the Page with Users;

## Getting started

Basic Requirements and Dependencies (observe pom.xml):

- Java (jdk-1.8)
- Maven
- Cucumber
- Serenity
-
## Adding tests

To contribute with the project (add tests) make the next steps:
1) Open IDE and Clone project locally: git clone https://gitlab.com/alexandr.zayats/facesserenitycucumber.git
2) Create a new branch
3) Add new '.feature' in features package src/test/resources/features/
4) Implement all Steps and Methods in src/test/java/facesapi/steps/
5) Add API calls to src/main/java/com/facesapi/api/
6) Observe you have relations between steps and calls
7) Run test locally 'mvn clean verify'
8) Observe Test Report in: target/site/serenity/index.html
9) After successful run - Commit and Push your changes
10) Make sure pipeline run on GitLab passed
11) Observe Serenity Test Report on Gitlab (Go to CI/CD >> Jobs >> Select <job_id> >> Browse
12) Open (Target > Site > Serenity > index.html)